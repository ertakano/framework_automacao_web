package br.com.framework_automacao_web.pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.com.framework_automacao_web.utils.WebDriverUtils;

public class HomePageObject extends WebDriverUtils{
	
	private String url = "http://www.correios.com.br/para-voce";

	@FindBy(id = "portaltab-para-voce")
	private WebElement idButtonVoce;
	
	public void goToHome() {
		navigateTo(url);
	}
	
	/**
	 * Valida se os componentes de validação da tela estão presentes
	 * @return
	 * @throws Exception 
	 */
	public boolean isValida() throws Exception {
		//WebElement idButtonVoce = driver.findElement(By.id("portaltab-para-voce"));
		return containsElement(idButtonVoce);
	}
	
}
