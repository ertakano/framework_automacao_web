package br.com.framework_automacao_web.driverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.framework_automacao_web.enumerator.SelectBrowser;

public class DriverFactory {

	private static WebDriver driver = null;

	public static WebDriver createDriver(SelectBrowser browser) {

		switch (browser) {

		case CHROME:
			if (driver == null) {
				System.setProperty("webdriver.chrome.driver", "/Users/inmetrics/Desktop/drivers/chromedriver");
				driver = new ChromeDriver();
			}
			return driver;

		case FIREFOX:
			if (driver == null) {
				System.setProperty("webdriver.gecko.driver", "/Users/inmetrics/Desktop/drivers/geckodriver");
				driver = new FirefoxDriver();
			}
			return driver;

		default:
			System.out.printf("Digite as opções de browser: CHROME ou FIREFOX.");
		}
		return driver;
	}

	public static WebDriver getDriver() {
		return driver;
	}
	
	public static WebDriver quitDriver() {
		if(driver != null) {
			driver.quit();
			driver = null;
		}
		return driver;
	}

}
