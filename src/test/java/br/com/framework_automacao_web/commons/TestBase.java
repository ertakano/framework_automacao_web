package br.com.framework_automacao_web.commons;

import java.io.IOException;

import br.com.framework_automacao_web.driverfactory.DriverFactory;
import br.com.framework_automacao_web.enumerator.SelectBrowser;

public class TestBase {

	public static void getDriver() throws IOException {
		DriverFactory.createDriver(SelectBrowser.CHROME);
		getDriver();
		System.out.println("Inicializando driver...");
	}

	public static void quitDriver() {
		System.out.println("Fechando driver....");
		quitDriver();
	}

}
